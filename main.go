package main

import (
	"fmt"
	"math"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func convUnit(data float64) (float64, string) {
	dout := data
	uout := "K"
	if dout >= 1024 {
		dout /= 1024
		uout = "M"
	}
	if dout >= 1024 {
		dout /= 1024
		uout = "G"
	}
	return dout, uout
}

func main() {
	out, err := exec.Command("free").Output()
	if err != nil {
		fmt.Printf("err\n")
		return
	}
	data := strings.Split(string(out), "\n")
	mem := strings.TrimSpace(data[1])
	memdata := strings.Split(mem, " ")
	memdatax := []string{}
	for i := range memdata {
		if memdata[i] == "" {
			continue
		}
		memdatax = append(memdatax, memdata[i])
	}

	tmem, _ := strconv.ParseFloat(memdatax[1], 64)
	umem, _ := strconv.ParseFloat(memdatax[2], 64)
	//	fmem, _ := strconv.ParseFloat(memdatax[6], 64)
	memc := int64(math.Round(umem / tmem * 100))

	tmemx, tmemu := convUnit(tmem)
	umemx, umemu := convUnit(umem)
	//	fmemx, fmemu := convUnit(fmem)

	if len(os.Args) == 1 {
		fmt.Printf("%0.2f%sB / %0.2f%sB", umemx, umemu, tmemx, tmemu)
	} else if os.Args[1] == "p" {
		fmt.Printf("%d", memc)
	} else if os.Args[1] == "b" {
		fmt.Printf(" %0.2f %sB (%d%%) ", umemx, umemu, memc)
	} else {
		fmt.Printf("error")
	}
}
